package talent.jump.data

data class ResetResponse(
    val data: ResetData,
    val status: Boolean
)