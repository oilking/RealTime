package talent.jump.data

data class CreateStreamResponse(
    val data: CreateStreamData,
    val status: Boolean
)