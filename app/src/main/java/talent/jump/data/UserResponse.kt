package talent.jump.data

data class UserResponse(
    val data: UserData,
    val status: Boolean
)