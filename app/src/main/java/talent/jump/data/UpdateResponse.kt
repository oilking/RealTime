package talent.jump.data

data class UpdateResponse(
    val data: UpdateData,
    val status: Boolean
)